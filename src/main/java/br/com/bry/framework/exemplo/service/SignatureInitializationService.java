package br.com.bry.framework.exemplo.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

import br.com.bry.framework.exemplo.configuration.ServiceConfiguration;
import br.com.bry.framework.exemplo.models.InitializationResponse;

@Component
public class SignatureInitializationService {

	@Autowired
	private RestTemplate restTemplate;

	/**
	 * Performs communication with the Signature API endpoint
	 * 
	 * @param request
	 * @return Initialization response from Signature API
	 * @throws Exception
	 */
	public InitializationResponse initializeSignature(HttpServletRequest request) throws Exception {
		ResponseEntity<InitializationResponse> responseInitialize = null;

		HttpEntity<?> requestToAPI = this.getHttpEntity(request);

		responseInitialize = restTemplate.exchange(ServiceConfiguration.INITIALIZE_SERVICE_URL, HttpMethod.POST,
				requestToAPI, InitializationResponse.class);

		return responseInitialize.getBody();
	}

	/**
	 * Analyze the request received from the front-end and configure the request
	 * that will be sent to the Signature API endpoint
	 * 
	 * @param request
	 * @return Request to the Signature API endpoint
	 */
	private HttpEntity<?> getHttpEntity(HttpServletRequest request) throws JSONException {
		List<MultipartFile> currentDocumentStreamContentValue = ((StandardMultipartHttpServletRequest) request)
				.getMultiFileMap().get("documento");

		Resource resourceOriginalDocument = null;

		if (currentDocumentStreamContentValue != null && !currentDocumentStreamContentValue.isEmpty()) {
			resourceOriginalDocument = currentDocumentStreamContentValue.get(0).getResource();
		}

		Resource resourceImage = null;

		List<MultipartFile> currentImageStreamContentValue = ((StandardMultipartHttpServletRequest) request)
				.getMultiFileMap().get("imagem");

		if (currentImageStreamContentValue != null && !currentImageStreamContentValue.isEmpty()) {
			resourceImage = currentImageStreamContentValue.get(0).getResource();
		}

		JSONObject dataToInitialize = new JSONObject();

		List<String> listaNonce = new ArrayList<>();
		listaNonce.add(request.getParameterValues("nonces")[0]);
		JSONArray noncesArray = new JSONArray(listaNonce);

		dataToInitialize.put("perfil", request.getParameterValues("perfil")[0]);
		dataToInitialize.put("algoritmoHash", request.getParameterValues("algoritmoHash")[0]);
		dataToInitialize.put("formatoDadosEntrada", request.getParameterValues("formatoDadosEntrada")[0]);
		dataToInitialize.put("formatoDadosSaida", request.getParameterValues("formatoDadosSaida")[0]);
		dataToInitialize.put("certificado", request.getParameterValues("certificado")[0]);
		dataToInitialize.put("nonces", noncesArray);
		
		String metadado = request.getParameterValues("Metadados")[0];
		JSONObject metaData = new JSONObject(metadado);
		JSONObject metadados = new JSONObject();
		metadados.put(metaData.getString("tipo"), "");
		metadados.put(metaData.getJSONObject("numero").getString("valor"), metaData.getJSONObject("numero").getString("numero"));
		metadados.put(metaData.getJSONObject("UF").getString("valor"), metaData.getJSONObject("UF").getString("UF"));
		metadados.put(metaData.getJSONObject("especialidade").getString("valor"), metaData.getJSONObject("especialidade").getString("especialidade"));
		//Insere também os OIDs do farmaceutico vazios quando o profissional é Médico
		if (metaData.getJSONObject("numero").getString("valor").equals("2.16.76.1.4.2.2.1")) {
			metadados.put("2.16.76.1.4.2.3.1", "");
			metadados.put("2.16.76.1.4.2.3.2", "");
			metadados.put("2.16.76.1.4.2.3.3", "");
		}
		
		final HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", request.getHeader("Authorization"));
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();

		map.add("dados_inicializar", dataToInitialize.toString());
		map.add("metadados", metadados.toString());
		map.add("documento", resourceOriginalDocument);
		
		if (resourceImage != null) {
			JSONObject imageConfiguration = new JSONObject();
			imageConfiguration.put("altura", request.getParameterValues("altura")[0]);
			imageConfiguration.put("largura", request.getParameterValues("largura")[0]);
			imageConfiguration.put("posicao", request.getParameterValues("posicao")[0]);
			imageConfiguration.put("pagina", request.getParameterValues("pagina")[0]);
			map.add("configuracao_imagem", imageConfiguration.toString());
			map.add("imagem", resourceImage);
		}
		return new HttpEntity<>(map, headers);
	}
}
