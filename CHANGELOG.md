# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2020-06-23

### Added

- Examples of signature generation. 

[1.0.0]: https://gitlab.com/brytecnologia-team/integracao/api-assinatura/java/backend-assinatura-pdf-com-bry-extension/-/tags/1.0.0
