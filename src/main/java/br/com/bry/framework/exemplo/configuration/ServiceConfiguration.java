package br.com.bry.framework.exemplo.configuration;

public class ServiceConfiguration {

	private ServiceConfiguration() {

	}

	public static final String SERVICE_BASE_URL = "https://hub2.bry.com.br";
	public static final String INITIALIZE_SERVICE_URL = SERVICE_BASE_URL
			+ "/fw/v1/pdf/pkcs1/assinaturas/acoes/inicializar";
	public static final String FINALIZE_SERVICE_URL = SERVICE_BASE_URL + "/fw/v1/pdf/pkcs1/assinaturas/acoes/finalizar";
}
