## Inclusão de Metadados em Prescrição Eletrônica com Assinatura PDF via BRy Extension

Este é um exemplo de integração dos serviços da API de assinatura com clientes baseados em tecnologia Java para geração de assinatura PDF. 

Este exemplo é integrado com o [frontend] de geração de assinatura. 

A aplicação realiza comunicação com o serviço de assinatura do Framework.

O documento será assinado em conjunto com a inclusão dos metadados.

### Autenticação, OID's e seus respectivos valores

Para entendimento dos requisitos de Autenticação, recomendamos que leia o artigo [Assinatura PDF Etapa Única](https://api-assinatura.bry.com.br/api-assinatura-digital#assinatura-pdf-etapa-unica).

Os valores correspondentes a cada tipo de OID podem ser encontrados no endereço [API Assinatura Prescrição](https://api-assinatura.bry.com.br/api-assinatura-medica#assinatura-da-prescricao).

### Tech

O exemplo utiliza a biblioteca Java abaixo:
* [JDK 8] - Java 8
* [Spring-boot] - Spring Boot makes it easy to create stand-alone, production-grade Spring based Applications that you can "just run".

### Uso

Para execução da aplicação de exemplo, importe o projeto em sua IDE de preferência (utilizamos o Eclipse).

Executar programa:

	clique com o botão direito em cima do arquivo PDFSignatureApplication.java -> Run as -> Java Application

   [JDK 8]: <https://www.oracle.com/java/technologies/javase-jdk8-downloads.html>
   [Spring-boot]: <https://spring.io/projects/spring-boot>
   [frontend]: <https://gitlab.com/brytecnologia-team/integracao/api-assinatura-medica/java/frontend-geracao-de-assinatura-medica-de-prescricao-eletronica-com-bry-extension>
   
