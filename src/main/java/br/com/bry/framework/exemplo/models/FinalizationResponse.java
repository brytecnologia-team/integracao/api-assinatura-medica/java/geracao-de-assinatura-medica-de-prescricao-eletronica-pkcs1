package br.com.bry.framework.exemplo.models;

import java.util.List;

public class FinalizationResponse {

	private List<Document> documentos;

	private String identificador;

	private int quantidadeAssinaturas;

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public int getQuantidadeAssinaturas() {
		return quantidadeAssinaturas;
	}

	public void setQuantidadeAssinaturas(int quantidadeAssinaturas) {
		this.quantidadeAssinaturas = quantidadeAssinaturas;
	}

	public List<Document> getDocumentos() {
		return documentos;
	}

	public void setDocumentos(List<Document> documentos) {
		this.documentos = documentos;
	}

	@Override
	public String toString() {
		return "FinalizationResponse [documentos=" + documentos + "]";
	}
}
